package gov.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.gov.uk/")
class GovUkHomePage extends PageObject {}
