package gov.navigation;

import org.openqa.selenium.By;

class NavigationSelectors {

    static By CLICK_FIRST_RESULT = By.cssSelector("#js-results > div > ol > li:nth-child(1) > a");
    static By SEARCH_CLICK = By.cssSelector("#get-started > a");
    static By SELECT_NEXT_STEP = By.cssSelector("#current-question > button");

    static By GET_DECISION = By.className("gem-c-heading");

}
