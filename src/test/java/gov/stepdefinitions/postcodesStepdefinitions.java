package gov.stepdefinitions;

import io.cucumber.java.en.When;
import gov.api.RequestResponse;
import net.thucydides.core.annotations.Steps;

public class postcodesStepdefinitions {

    @Steps
    private
    RequestResponse requestResponse;

    @When("I send a get request to \"(.*)\" and get a \"(.*)\" response")
    public void iSendAGetRequestTo(String postcode, int response) {
        requestResponse.apiSendAndReceive(postcode, response);

    }

}