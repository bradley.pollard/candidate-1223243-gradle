package gov.travelling;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class TravellingWith extends UIInteractionSteps {

    @Step("Selection")
    public void govUkSelection(String selection) {
        System.out.println(selection);
        if (selection.equals("no")) {
            $(TravellingSelectors.SELECT_OPTION_TWO).click();
        } else {
            $(TravellingSelectors.SELECT_OPTION_ONE).click();
        }
    }
}