package gov.search;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class SearchFor extends UIInteractionSteps {

    @Step("Select search box and enter search term")
    public void govUkSelectAndSearch() {
        $(SearchForm.GET_SEARCH_BOX).typeAndEnter("check uk visa");
    }

}