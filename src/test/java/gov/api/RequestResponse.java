package gov.api;

import io.restassured.http.ContentType;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class RequestResponse extends UIInteractionSteps {

    @Step("Send request and receive response")
    public void apiSendAndReceive(String postcode, int response) {
        given().contentType(ContentType.JSON)
                .when().get("http://api.postcodes.io/postcodes/" + postcode)
                .then().body("status", equalTo(response));
    }

}
