# Candidate-1223243

## Technologies Used

* Serenity (Cucumber framework)
* Gradle
* Rest Assured
* Hamcrest
* Zalenium (Selenium)
   

## To Run

* Run `./zalenium-starter.sh`

or 
* Run `CucumberTestSuite`

or 
* `gradle test`

## Evidence

![alt text](Passing-Tests.png "Passing Tests")

